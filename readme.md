# credit-debit api

This project implements an in-memory credit-debit api

It provides a `Swagger-UI` application in `/api-docs` path to test

The app uses default port `8080` but it can be override with environment variable `PORT`


# run app
```
npm start
```

# create production build
```
npm run build-prod
```
