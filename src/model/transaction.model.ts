import { type } from './type';

export interface ITransaction {
    id?: string;
    type: type;
    amount: number;
    effectiveDate?: Date;
}

export class Transaction implements ITransaction  {
    id: string;
    type: type;
    amount: number;
    effectiveDate: Date;
}
