import { type } from './type';

export interface ITransactionBody {
    type: type;
    amount: number;
}

export class TransactionBody implements ITransactionBody  {

    type: type;
    amount: number;

}
