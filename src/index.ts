import express from 'express';
import cors from 'cors';
import swaggerUi from 'swagger-ui-express';
import bodyParser from 'body-parser';
import { v1 } from 'uuid';
import schema from './schema.json';
import { Transaction, ITransaction } from './model/transaction.model';
import { ITransactionBody } from './model/transaction-body.model';
import { type } from './model/type';

const port = process.env.PORT || 8080;
const allowedOrigins = process.env.ALLOWED_ORIGINS || "*";
const app = express();

const transactions: { [id:string]: ITransaction } = {}
let balance = 0;

app.use(cors({
    origin: allowedOrigins
}))
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(schema));
app.use(bodyParser.json())

app.get('/balance', (req, res)=>{
    res.send({
        current: balance
    })
})

app.get('/transactions', (req, res)=>{
    const result: ITransaction[] = [];
    for(const transactionId in transactions){
        if(transactions.hasOwnProperty(transactionId)){
            result.push(transactions[transactionId])
        }
    }
    res.send(result);
})

app.get('/transactions/:transactionId', (req, res)=>{
    const result: ITransaction = transactions[req.params.transactionId]
    if(typeof result === 'object' ){
        res.send(result);
    }
    else{
        res.status(404).send();
    }
})

app.post('/transactions', (req, res)=>{
    const transactionBody:ITransactionBody = req.body as ITransactionBody;
    if(transactionBody.type === type.credit){
        balance += transactionBody.amount;
    }
    else {
        if(balance >=  transactionBody.amount){
            balance -= transactionBody.amount;
        }
        else{
            res.status(400).send();
            return;
        }
    }
    const transaction:ITransaction = new Transaction();
    transaction.id = v1();
    transaction.type = transactionBody.type;
    transaction.amount = transactionBody.amount;
    transaction.effectiveDate = new Date();
    transactions[transaction.id] = transaction;
    res.send(transaction)
})

// start the Express server
app.listen( port, () => {
    console.log( `server started at port: ${ port }` );
});




